<?php
    include 'constants.php';
    include 'uploads.php';

    function empty_user_db()
    {
        if(file_exists(DB_PATH) && filesize(DB_PATH) != 0)
            return false;
        else
            return true;
    }

    function db_users_to_page()
    {
        $db_str = file_get_contents(DB_PATH);
        $db_entries = explode("\n", $db_str);
        array_pop($db_entries);

        echo "<table>";
        foreach($db_entries as $entry)
        {
            echo "<tr>\n";
            $fields = explode(",", $entry);
            echo "<td>";

            if(count($fields) > count(MANDATORY_FIELDS))
                place_avatar(array_pop($fields));
            else
                place_avatar(AVATAR_ANON);

            echo "</td>\n";

            foreach($fields as $fd)
                echo "<td>$fd</td>\n";

            echo "</tr>\n";
        }
        echo "</table>\n";
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie-edge" />
    <link rel="stylesheet" href="assets/css/materialize.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <style>
        .container{
            width: 400px;
        }
    </style>
</head>
<body style=" padding-top: 3rem; ">
    <div class="container">
        <?php
           if(!empty_user_db())
                db_users_to_page(DB_PATH);
            else
                echo "No users";
        ?>
        <br />
        <a href="handler.php" class="btn">return back</a>
    </div>
</body>
</html>