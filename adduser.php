<!doctype html>
<html lang=en>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="ie-edge" />
	<link rel="icon" type="image/x-icon" href="favicon.ico" />
	<link rel="stylesheet" href="assets/css/materialize.min.css" />
	<style>
		body{
			padding-top: 3rem;
		}
		.container{
			width: 400px;
		}
	</style>
</head>
<body>
	<div class="container">
		<h3>Add New User</h3>
		<form action="handler.php" method=post enctype="multipart/form-data">
			<div class="row">
				<div class="field">
					<label>Name: <input required type="text" name="name" /></label>
				</div>
			</div>
			<div class="row">
				<div class="field">
					<label>E-mail: <input required type="email" name="email" /><br /></label>
				</div>
			</div>
			<div class="row">
				<div class="field">
					<label>
						<input required class="with-gap" name="gender" value="male" type="radio" />
						<span>Male</span><br />
					</label>
				</div>
			</div>
			<div class="row">
				<div class="field">
					<label>
						<input required class="with-gap" name="gender" value="female" type="radio" />
						<span>Female</span><br />
					</label>
				</div>
			</div>
			<div class="row">
				<div class="file-field input-field">
					<div class="btn">
						<span>Photo</span>
						<input type="file" name="avatar" accept="image/jpeg,image/png,image/gif" />
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text" />
					</div>
				</div>
			</div>
			<input type="submit" class="btn" value="Add" />
		</form>
	</div>
</body>
</html>