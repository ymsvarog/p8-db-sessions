<?php
    define("DB_PATH", "database/users.csv");
    define("MANDATORY_FIELDS", ["name", "email", "gender"]);
    define("AVATARS_PATH", "public/avatars");
    define("AVATAR_ANON", AVATARS_PATH . "/anon.jpeg");
?>